public class MainApp
{
	public static void main(String[] args){
		MusicCup mCup = new MusicCup();
		mCup.addWater(20);
		mCup.play();
		mCup.drinkWater(18);
		mCup.play();
	}
}

interface Cup{
	void addWater(int w);
	void drinkWater(int w);
}

interface MusicPlay{
	void play();
}

class MusicCup implements Cup, MusicPlay
{
	public void addWater(int w){
		this.water += w;
		System.out.println(this.water);
	}
	public void drinkWater(int w){
		this.water -= w;
		System.out.println(this.water);
	}
	public void play(){
		System.out.println("La..la..la...");
	}
	private int water = 0;
}