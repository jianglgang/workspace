package com.znb.person;
public class Human
{
	public Human(int h){
		this.height = h;
	}
	public int getHeight(){
		return this.height;
	}
	public void growHeight(int h){
		this.height = this.height+h;
	}
	public void breath(){
		System.out.println("hu...hu...");
	}
	private int height;
}