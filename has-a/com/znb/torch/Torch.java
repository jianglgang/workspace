package com.znb.torch;
// import .Battery;
public class Torch
{
	public void turnOn(int hours){
		boolean usable;
		usable = this.theBattery.useBattery(hours*0.1);
		if(usable != true){
			System.out.println("No more usable, must charge!");
		}
	}
	public void charge(int hours){
		this.theBattery.chargeBattery(hours*0.2);
	}
	private Battery theBattery = new Battery();
}